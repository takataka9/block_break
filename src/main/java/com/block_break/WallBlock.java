package com.block_break;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WallBlock implements FieldObject {
    private Point pt;
    private boolean leftRight;
    private boolean top;
    private boolean bottom;

    private WallBlock(int x, int y, boolean leftRight, boolean top, boolean bottom){
        pt = new Point(x, y);
        this.leftRight = leftRight;
        this.top = top;
        this.bottom = bottom;
    }

    public static List<WallBlock> createBlocks(int height, int width){
        List<WallBlock> walls = new ArrayList<>();
        for(int i=1;i<width;i++){
            if(i == 1 || i == width-1){
                walls.add(new WallBlock(i, 3, true, true, false));
                walls.add(new WallBlock(i, height-1, true, false, true));
                continue;
            }
            walls.add(new WallBlock(i, 3, false, true, false));
            walls.add(new WallBlock(i, height-1, false, false, true));
        }
        for(int i=4;i<height-1;i++){
            walls.add(new WallBlock(1, i, true, false, false));
            walls.add(new WallBlock(width-1, i, true, false, false));
        }

        return walls;
    }

    @Override
    public int getLength(){
        return 1;
    }

    @Override
    public void collision(Ball ball){
        if(bottom){
            End.run();
        }

        if(top){
            ball.reverseVecY();
        }
        if(leftRight){
            ball.reverseVecX();
        }

        ball.collision(this, 0);
    }

    @Override
    public String putObject(){
        return "-";
    }

    @Override
    public int getX() {
        return pt.getX();
    }

    @Override
    public int getY() {
        return pt.getY();
    }

    @Override
    public Point getPoint() {
        return pt;
    }

    @Override
    public List<Point> getArea(){
        return Arrays.asList(pt);
    }
}
