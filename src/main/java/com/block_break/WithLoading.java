package com.block_break;

import com.block_break.hobby.Loading;

public class WithLoading implements Contents {
    public WithLoading(){

    }

    @Override
    public String contentsName(){
        return "Start";
    }

    @Override
    public void run() {
        Loading.run();
        try {
            Thread.sleep(500);
        }catch(InterruptedException ie){

        }

        Game.run();
    }
}
