package com.block_break.samples;

import jline.Terminal;
import jline.TerminalFactory;
import jline.console.ConsoleReader;
import com.block_break.Ball;
import com.block_break.Point;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

public class SampleBall {
    public static Terminal terminal;
    public static int height;
    public static int width;
    public static Ball ball;

    public static void main(String[] args){
        terminal = TerminalFactory.get();
        height = terminal.getHeight();
        width = terminal.getWidth();
        ball = new Ball(1, 1);

        try{
            ConsoleReader console = new ConsoleReader();
            while(true){
                console.clearScreen();
                console.flush();
                ball.move();
                collision();

                showDisplay();
                try {
                    Thread.sleep(50);
                }catch ( InterruptedException ie){
                    ie.printStackTrace();
                }
            }

        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }


    public static void showDisplay(){
        for(int i=0;i<height;i++){
            if(i == ball.getY()){
                String left = StringUtils.repeat(' ', ball.getX()-1);
                String right = StringUtils.repeat(' ', width-ball.getX()-1);
                System.out.println(left + "*" + right);
            }else{
                System.out.println("");
            }
        }
    }

    public static void collision(){
        Point next = ball.getNextPoint();
        int next_x = next.getX();
        int next_y = next.getY();

        if((next_x < 1 || width - 1 < next_x) && (next_y < 0 || height < next_y )){
            ball.changeDirection();
            return;
        }
        if(next_x < 1|| width - 1< next_x){
            ball.changeDirection();
            return;
        }
        if(next_y < 0 || height < next_y){
            ball.changeDirection();
            return;
        }
    }
}
