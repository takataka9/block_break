package com.block_break.samples;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SampleComparaltor implements Comparator<Integer> {
    public int compare(Integer num1, Integer num2){
        return num1 - num2;
    }

    public static void main(String[] args){
        List<Integer> list = Arrays.asList(1,4,8,9,2,6,3,8);
        Comparator<Integer> comparator = new SampleComparaltor();

        System.out.println(list);

        list.sort(comparator);
        System.out.println(list);
    }
}
