package com.block_break.samples;

import java.util.Arrays;
import java.util.List;

public class SampleArrays {
    public static void main(String[] args){
        List<List<Integer>> arys = Arrays.asList(
                Arrays.asList(1,2,3,4,5),
                Arrays.asList(6,7,8,9,10),
                Arrays.asList(11,12,13,14,15),
                Arrays.asList(16,17,18,19,20)
        );

        for(List<Integer> list: arys){
            for(Integer num: list){
                System.out.print(num + " ");
            }
            System.out.println("");
        }
    }
}
