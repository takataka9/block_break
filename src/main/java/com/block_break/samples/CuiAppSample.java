package com.block_break.samples;



import jline.console.ConsoleReader;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

public class CuiAppSample {
    public static void main(String[] args){
        ConsoleReader console = null;
        char txt = 'a';
        try {
            console = new ConsoleReader();
        }catch(IOException e){
            e.printStackTrace();
        }

        while(true){
            try {
                console.clearScreen();
                console.flush();
            }catch(IOException e){
                e.printStackTrace();
            }
            printStates();
            printTxt(txt);
            txt = (char)((int)txt + 1);
            try{
                Thread.sleep(500);
            }catch(InterruptedException ie){
                ie.printStackTrace();
            }
        }
    }
    public static void printStates(){
        System.out.println("HelloWorldHelloWorldHelloWorld");
        System.out.println("------------------------------");
    }
    public static void printTxt(char c){
        int i = 0;
        String rep = StringUtils.repeat(c, 20);
        while(i < 10) {
            System.out.println(rep);
            i++;
        }
    }
}




