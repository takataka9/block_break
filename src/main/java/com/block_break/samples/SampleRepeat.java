package com.block_break.samples;

import org.apache.commons.lang3.StringUtils;

public class SampleRepeat {
    public static void main(String[] args){
        System.out.println(StringUtils.repeat('a', 5));

        System.out.println(StringUtils.repeat('b', 0));

        System.out.println(StringUtils.repeat('c', 5));
    }
}
