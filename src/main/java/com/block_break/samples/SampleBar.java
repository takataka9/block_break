package com.block_break.samples;

import jline.Terminal;
import jline.TerminalFactory;
import jline.console.ConsoleReader;
import com.block_break.Ball;
import com.block_break.Controller;
import com.block_break.PlayerBlock;
import org.apache.commons.lang3.StringUtils;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;



import java.io.IOException;
import java.util.logging.LogManager;

public class SampleBar{
    public static Terminal terminal;
    public static int height;
    public static int width;
    public static PlayerBlock player;
    public static Ball ball;

    public static void main(String[] args){
        terminal = TerminalFactory.get();
        height = terminal.getHeight();
        width = terminal.getWidth();
        player = new PlayerBlock(7, width/2, (int)(height*0.9));
        ball = new Ball(width/2, (int)(height*0.6));

        if(!GlobalScreen.isNativeHookRegistered()){
            try{
                GlobalScreen.registerNativeHook();
            }catch(NativeHookException nhe){
                nhe.printStackTrace();
                System.exit(-1);
            }
        }
        GlobalScreen.addNativeKeyListener(new Controller(player, width));
        LogManager.getLogManager().reset();


        try {
            ConsoleReader console = new ConsoleReader();
            while(true){
                console.clearScreen();
                console.flush();
                showDisplay();

                try{
                    Thread.sleep(50);

                }catch(InterruptedException ie){
                    ie.printStackTrace();
                }
            }
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public static void showDisplay(){
        for(int i=0;i<height;i++){
            if(i == player.getY()){
                String left = StringUtils.repeat(' ', player.getX()-player.getLength()/2);
                String right =StringUtils.repeat(' ', width - player.getX() + player.getLength()/2);
                System.out.println(left + player.putObject() + right);
            }else{
                System.out.println("");
            }

        }
    }
}
