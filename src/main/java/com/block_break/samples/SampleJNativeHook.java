package com.block_break.samples;

import jline.console.ConsoleReader;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.io.IOException;
import java.util.logging.LogManager;

public class SampleJNativeHook {
    public static void main(String[] args) throws IOException {
        //フックされていなかったら
        if(!GlobalScreen.isNativeHookRegistered()){
            try{
                //フックを登録
                GlobalScreen.registerNativeHook();
            }catch (NativeHookException e){
                e.printStackTrace();
                System.exit(-1);
            }
        }

        ConsoleReader console = new ConsoleReader();

        //キー・リスナを登録
        NativeKeyListener listener = new NativeKeyListener(){

            //キーを押した時
            @Override
            public void nativeKeyPressed(NativeKeyEvent e){

            }

            //キーを離した時
            @Override
            public void nativeKeyReleased(NativeKeyEvent e){
            }

            //キーをタイプしたとき
            @Override
            public void nativeKeyTyped(NativeKeyEvent e){
                System.out.println(e.getRawCode());
            }


        };
        GlobalScreen.addNativeKeyListener(listener);
        LogManager.getLogManager().reset();

    }
}


