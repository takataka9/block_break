package com.block_break;

public class Start implements Contents {
    public Start(){}

    @Override
    public String contentsName() {
        return "Start without Loading";
    }

    @Override
    public void run() {
        Game game = new Game();

        game.run();
    }
}
