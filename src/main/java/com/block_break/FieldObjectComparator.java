package com.block_break;

import java.util.Comparator;

public class FieldObjectComparator implements Comparator<FieldObject> {
    public int compare(FieldObject ob1, FieldObject ob2){
        if(ob1.getY() != ob2.getY()){
            return ob1.getY() - ob2.getY();
        }

        return ob1.getX() - ob2.getX();
    }
}
