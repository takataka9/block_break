package com.block_break;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class PlayerBlock implements FieldObject {
    private String bar;
    private Point pt;
    private List<Point> points;
    private int length;
    private double degreeParX;


    public PlayerBlock(int length, int center_x, int y){
        this.length = length;
        this.pt = new Point(center_x, y);
        this.points = new ArrayList<>();
        for(int i=pt.getX()-length/2;i<=pt.getX()+length/2;i++){
            points.add(new Point(i, y));
        }
        bar = "-" + StringUtils.repeat('=', length-2) + "-";
        degreeParX = 1.0 / (length/2.0);

    }

    @Override
    public int getLength(){
        return this.length;
    }

    @Override
    public void collision(Ball ball){
        int dx = ball.getX() - pt.getX();

        ball.reverseVecY();
        ball.changeVecX(dx * degreeParX);
        ball.collision(ball);
    }

    @Override
    public String putObject(){
        return bar;
    }

    @Override
    public int getX(){
        return pt.getX();
    }

    @Override
    public int getY(){
        return pt.getY();
    }

    @Override
    public Point getPoint(){
        return pt;
    }

    @Override
    public List<Point> getArea(){
        return points;
    }

    public Point getLeft(){
        return points.get(0);
    }

    public Point getRight(){
        return points.get(points.size()-1);
    }


    public void move(int direction){
        this.pt.moveX(direction);
        for(Point pt: points){
            pt.moveX(direction);
        }
    }
}
