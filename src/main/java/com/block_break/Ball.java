package com.block_break;

import java.util.Arrays;
import java.util.List;

public class Ball implements FieldObject{
    private double vec_x;
    private double vec_y;
    private double meter;
    private Point pt;
    private Direction dir;
    private Point next;

    public Ball(int x, int y){
        vec_x = 0;
        vec_y = 1;
        meter = 0;
        pt = new Point(x, y);
        dir = Direction.DOWN;
        this.next();
    }

    public void changeDirection(){
        /*方向変化の処理*/
        if(vec_y == 1) {
            dir = Direction.DOWN;
            return;
        }
        dir = Direction.UP;
    }

    @Override
    public int getLength(){
        return 1;
    }

    @Override
    public String putObject() {
        return "*";
    }

    @Override
    public void collision(Ball ball){
        changeDirection();
        next();
    }

    public void collision(FieldObject obj, double num) {
        if(obj instanceof PlayerBlock){ // player と衝突した場合
            vec_x = num;
        }
        changeDirection();
        next();
    }
    @Override
    public int getX(){
        return this.pt.getX();
    }
    @Override
    public int getY(){
        return this.pt.getY();
    }
    @Override
    public Point getPoint(){
        return pt;
    }

    @Override
    public List<Point> getArea(){
        return Arrays.asList(pt);
    }

    public void reverseVecX(){
        vec_x = vec_x * (-1);
    }
    public void reverseVecY(){
        vec_y = vec_y * (-1);
    }
    public void changeVecX(double num){ vec_x = num;}

    public Point getNextPoint(){
        return next;
    }

    public void move(){
        pt = next;
    }

    public void next(){
        if(meter > 1.0){
            meter += Math.abs(vec_x) - 1.0;
            next = dir.shift(vec_x).next(pt);
            return;
        }
        meter += Math.abs(vec_x);
        next = dir.next(pt);
    }
}
