package com.block_break;

import jline.Terminal;
import jline.TerminalFactory;
import jline.console.ConsoleReader;
import org.apache.commons.lang3.StringUtils;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.logging.LogManager;

public class Game {
    private Terminal terminal;
    private List<FieldObject> fieldObjects;
    private int height;
    private int width;
    private Ball ball;
    private PlayerBlock player;
    private int targetCount;


    public Game() {
        terminal = TerminalFactory.get();
        height = terminal.getHeight();
        width = terminal.getWidth();
        ball = new Ball(width / 2, (int)(height*0.6));
        player = new PlayerBlock(13, width/2, (int)(height*0.9));
        fieldObjects = new ArrayList<>();
        fieldObjects.addAll(Arrays.asList(ball, player));
        fieldObjects.addAll(WallBlock.createBlocks(height, width));
        List<TargetBlock> targets = TargetBlock.createBlocks(height, width, 0);
        targetCount = targets.size();
        fieldObjects.addAll(targets);
    }

    public void showDisplay() {
        int cntObj = 0;
        for(int i=0;i<height;i++){
            String line="";
            if(cntObj < fieldObjects.size()) {
                FieldObject obj;
                for (int j = cntObj; j < fieldObjects.size() && (obj = fieldObjects.get(j)).getY() == i; j++) {
                    try {
                        line = line + StringUtils.repeat(' ', obj.getX() - line.length() - obj.getLength() / 2);
                    }catch(NegativeArraySizeException nase){
                        System.out.println(obj.getX() - line.length() - obj.getLength()/2);

                    }
                    line = line + obj.putObject();
                    cntObj++;
                }
            }
            System.out.println(line);
        }
    }

    public static void run() {
        try {
            ConsoleReader console = new ConsoleReader();
            Game game = new Game();
            if(!GlobalScreen.isNativeHookRegistered()){
                try {
                    GlobalScreen.registerNativeHook();
                }catch (NativeHookException nhe){
                    nhe.printStackTrace();
                }
            }

            Ball ball = null;

            for(FieldObject obj: game.fieldObjects){
                if(obj instanceof PlayerBlock) {
                    GlobalScreen.addNativeKeyListener(new Controller((PlayerBlock) obj, game.width));
                    break;
                }
                if(obj instanceof Ball){
                    ball = (Ball)obj;
                }
            }

            LogManager.getLogManager().reset();
            Comparator<FieldObject> comparator = new FieldObjectComparator();

            while(true) {
                console.clearScreen();
                console.flush();
                game.fieldObjects.sort(comparator);
                game.showDisplay();
                ball.next();
                game.collisionCheck();
                ball.move();

                try {
                    Thread.sleep(50);
                }catch (InterruptedException ie){
                    ie.printStackTrace();
                    break;
                }
            }
            System.exit(0);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void collisionCheck(){
        Point pt = ball.getNextPoint();
        FieldObject fobj = null;
        for(FieldObject obj: fieldObjects){
            if(obj.getArea().contains(pt)){
                if(obj instanceof TargetBlock){
                    fobj = obj;
                    targetCount--;
                }
                obj.collision(ball);
            }
        }
        fieldObjects.remove(fobj);
        if(targetCount == 0){
            End.clear();
        }

    }

    public static void main(String[] args){
        Game.run();
    }
}


