package com.block_break;

import jline.Terminal;
import jline.TerminalFactory;
import jline.console.ConsoleReader;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class End {
    private Terminal terminal;
    private int height;
    private int width;
    private static List<String> explosion1 = Arrays.asList(
            "","","","","","","","","","","","","","",
            "　　　＼　　　　　　☆",
            "　　　　　　　　　　　|　　　　　☆",
            "　　　　　　　(⌒ ⌒ヽ　　　/",
            "　　＼　　（´⌒　　⌒　　⌒ヾ　　　／",
            "　　　 （'⌒　;　⌒　　　::⌒　　）",
            "　　　（´　　　　　）　　　　　:::　）　／",
            "☆─　（´⌒;:　　　　::⌒`）　:;　　）",
            "　　　（⌒::　　　::　　　　　::⌒　）",
            " 　／　（　　　　ゝ　　ヾ　丶　　ソ　─");
    private static List<String> explosion2 = Arrays.asList(
            "　　　　,,-'　 _,,-''\"　　　　　　\"''- ,,_　 ￣\"''-,,__　 ''--,,__",
            "　　　　　　　　,,-''\"　　,, --''\"ニ_―- _　　''-,,_　　　 ゞ　　　　\"-",
            "　　　　　　 て　　　／ ,,-\",-''i|　　￣|i''-､　　ヾ　　　{",
            "　　　　　 （\"　　.／ 　 i　{;;;;;;;i|　　　 .|i;;;;;;）　,ノ　　　 ii",
            "　 ,,　　　　　　 （　　　 l,　`'-i|　　　　|i;;-'　　　　 ,,-'\"　　　_,,-",
            "　　\"'-,,　　　　　`-,,,,-'--'':::￣:::::::''ニ;;-==,_____　'\"　　_,,--''",
            "　　　　 ￣\"''-- _-'':::::\"￣::::::::::::::::;;;;----;;;;;;;;::::`::\"''::---,,_　",
            "　　　　　._,,-'ニ-''ニ--''\"￣.i|￣　　　|i-----,,￣`\"''-;;::''-`-,,",
            "　　　,,-''::::二-''\"　　　　 .--i|　　　　 .|i　　　　　　　　　 \"- ;;:::\\`､",
            "　._,-\"::::／　　　￣\"\'\'---　 i|　　　　　|i　　　　　　　　　　　　ヽ::::i",
            ".（:::::{:（i(____　　　　　　　　 i|　　　　　.|i　　　　　　　　　　_,,-':/:::}",
            "　 `''-,_ヽ:::::''- ,,__,,,,　_______i|　　　　　 .|i--__,,----..--'''\":::::ノ,,-'",
            "　　　　\"--;;;;;;;;;;;;;;;;;\"\"''--;;i|　　　　　　.|i二;;;;;::---;;;;;;;::--''\"~",
            "　　　　　　　　　　 ￣￣\"..i|　　　　 　　.|i",
            "　　　　　　　　　　　　　　.i|　　　　　　　 |i",
            "　　　　　　　　　　　　　　i|　　　　　　　　|i",
            "　　　　　　　　　　　　　 .i|　　 　 　　　　 .|i",
            "　　　　　　　　　　　　　.i|　　　　　　　　　..|i",
            "　　　　　　　　　　　　 .i|　　　　 　　　　　　|i",
            "　　　　　　　　　　　　.i|　　　　　　,,-､　､　　|i",
            "　　　　　　　　　　　 i|　　　　　　ノ::::i:::トiヽ､_.|i",
            "　　　　　　　　_,,　　i|/\"ヽ／:iヽ!::::::::ﾉ:::::Λ::::ヽ|i__n､ト､",
            "　　,,／^ヽ,-''\":::i／::::::::／:::::|i／;;;;;;/::::;;;;ノ⌒ヽノ::::::::::::ヽ,_Λ");
    private static List<String> end = Arrays.asList(
            "","","","","",
            "EEEEEEEEEEE  NNNNNN    NNNN  DDDDDDDDD",
            "EEEEEEEEEEE  NNNNNNN   NNNN  DDDDDDDDDDD",
            "EEEEEEEEEEE  NNNNNNN   NNNN  DDDDDDDDDDDD",
            "EEEE         NNNN NNN  NNNN  DDDD     DDDD",
            "EEEEEEEEEEE  NNNN NNNN NNNN  DDDD      DDDD",
            "EEEEEEEEEEE  NNNN NNNN NNNN  DDDD      DDDD",
            "EEEEEEEEEEE  NNNN NNNN NNNN  DDDD      DDDD",
            "EEEE         NNNN  NNNNNNNN  DDDD     DDDD",
            "EEEEEEEEEEE  NNNN   NNNNNNN  DDDDDDDDDDDD",
            "EEEEEEEEEEE  NNNN    NNNNNN  DDDDDDDDDD",
            "EEEEEEEEEEE  NNNN    NNNNNN  DDDDDDDD","","","","","");

    private static List<String> clear = Arrays.asList(
            "","","","","",
            " CCCC   LL    EEEE    AAAA    RRRRR",
            "CC  CC  LL    EEEE   AA  AA   RR  RR",
            "CC      LL    EE     AA  AA   RR  RR",
            "CC      LL    EEEE   AA  AA   RRRRR",
            "CC      LL    EEEE  AA    AA  RR  RR",
            "CC  CC  LL    EE    AA    AA  RR   RR",
            " CCCC   LLLL  EEEE  AA    AA  RR   RR");

    public End(){
        terminal = TerminalFactory.get();
        height = terminal.getHeight();
        width = terminal.getWidth();
    }

    public static void run(){
        try{
            ConsoleReader console = new ConsoleReader();
            End end = new End();

            console.clearScreen();
            console.flush();
            print(explosion1, 0);

            console.clearScreen();
            console.flush();
            print(explosion2, 0);

            console.clearScreen();
            console.flush();
            int rep = (end.width - End.end.get(7).length())/2;
            print(End.end, rep);


        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        System.exit(0);
    }

    public static void clear(){
        try{
            ConsoleReader console = new ConsoleReader();
            End end = new End();

            console.clearScreen();
            console.flush();
            int rep = (end.width - End.clear.get(7).length())/2;
            print(End.clear, rep);


        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        System.exit(0);
    }


    public static void print(List<String> strings, int rep){

        for(String str: strings){
            System.out.println(StringUtils.repeat(" ", rep)+str);
        }
        try{
            Thread.sleep(1000);
        }catch (InterruptedException ie){
            ie.printStackTrace();
        }
    }

    public static void main(String[] args){
        End.run();
    }
}
