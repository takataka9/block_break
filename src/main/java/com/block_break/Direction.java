package com.block_break;

public enum Direction implements Movable {
    UP(){
        @Override
        public Point next(Point p) {
            return new Point(p.getX(), p.getY()-1);
        }

        @Override
        public Direction shift(double vec) {
            if(vec < 0){
                return UP_LEFT;
            }
            return UP_RIGHT;
        }
    },
    UP_RIGHT(){
        @Override
        public Point next(Point p) {
            return new Point(p.getX()+1, p.getY()-1);
        }

        @Override
        public Direction shift(double vec) {
            return this;
        }
    },
    RIGHT(){
        @Override
        public Point next(Point p) {
            return new Point(p.getX()+1, p.getY());
        }

        @Override
        public Direction shift(double vec) {
            if(vec < 0){
                return UP_RIGHT;
            }
            return DOWN_RIGHT;
        }
    },
    DOWN_RIGHT(){
        @Override
        public Point next(Point p) {
            return new Point(p.getX()+1, p.getY()+1);
        }

        @Override
        public Direction shift(double vec) {
            return this;
        }
    },
    DOWN(){
        @Override
        public Point next(Point p) {return new Point(p.getX(), p.getY()+1);}

        @Override
        public Direction shift(double vec) {
            if(vec < 0){
                return DOWN_LEFT;
            }
            return DOWN_RIGHT;
        }
    },
    DOWN_LEFT(){
        @Override
        public Point next(Point p) {
            return new Point(p.getX()-1, p.getY()+1);
        }

        @Override
        public Direction shift(double vec) {
            return this;
        }
    },
    LEFT(){
        @Override
        public Point next(Point p) {
            return new Point(p.getX()-1, p.getY());
        }

        @Override
        public Direction shift(double vec) {
            if(vec < 0){
                return UP_LEFT;
            }
            return DOWN_LEFT;
        }
    },
    UP_LEFT(){
        @Override
        public Point next(Point p) {
            return new Point(p.getX()-1, p.getY()-1);
        }

        @Override
        public Direction shift(double vec) {
            return this;
        }
    };

}
