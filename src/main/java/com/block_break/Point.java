package com.block_break;

public class Point {
    private int x;
    private int y;

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public void moveX(int dx){ this.x += dx;}

    public void moveY(int dy){ this.y += dy;}

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Point)){
            return false;
        }

        return ((Point) o).getX() == x && ((Point) o).getY() == y;
    }
}
