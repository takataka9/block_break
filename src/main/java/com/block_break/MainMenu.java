package com.block_break;

import jline.Terminal;
import jline.TerminalFactory;
import jline.console.ConsoleReader;
import org.apache.commons.lang3.StringUtils;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.LogManager;

public class MainMenu implements NativeKeyListener {
    private Terminal terminal;
    private int height;
    private int width;
    private int cnt;
    private List<Contents> contents;
    private int select;
    private boolean flag;

    private static List<String> title = Arrays.asList(
            "","","","","",
            "BBBBB   LL     OOOO    CCCC   KK   KK",
            "BB  BB  LL    OO  OO  CC  CC  KK  KK",
            "BB  BB  LL    OO  OO  CC      KK KK",
            "BBBBBB  LL    OO  OO  CC      KKKK",
            "BBBBBB  LL    OO  OO  CC      KKKK",
            "BB  BB  LL    OO  OO  CC      KK KK",
            "BB  BB  LL    OO  OO  CC  CC  KK  KK",
            "BBBBB   LLLL   OOOO    CCCC   KK   KK");

    public MainMenu(){
        terminal = TerminalFactory.get();
        height = terminal.getHeight();
        width = terminal.getWidth();
        cnt = 0;
        contents = new ArrayList<>();
        contents.addAll(Arrays.asList(new WithLoading(), new Start()));
        select = 0;
        flag = true;

    }

    public static void run(){
        try{
            ConsoleReader console = new ConsoleReader();
            MainMenu mainMenu = new MainMenu();

            if(!GlobalScreen.isNativeHookRegistered()){
                try{
                    GlobalScreen.registerNativeHook();
                }catch (NativeHookException nhe){
                    nhe.printStackTrace();
                    System.exit(0);
                }
            }

            GlobalScreen.addNativeKeyListener(mainMenu);
            LogManager.getLogManager().reset();

            int rep = (mainMenu.width - title.get(5).length())/2;

            while(mainMenu.flag){
                console.clearScreen();
                console.flush();
                if(mainMenu.cnt < title.size()) {
                    mainMenu.cnt++;
                }
                for(int i=title.size()-mainMenu.cnt;i<title.size();i++){
                    System.out.println(StringUtils.repeat(" ", rep)+title.get(i));
                }
                for(int i=0;i<5;i++){
                    System.out.println("");
                }
                if(mainMenu.cnt == title.size()) {
                    for(int i=0;i<mainMenu.contents.size();i++){
                        Contents c = mainMenu.contents.get(i);
                        int repeat = (mainMenu.width - c.contentsName().length()) / 2;
                        if(i == mainMenu.select) {
                            System.out.println(StringUtils.repeat(" ", repeat-4) + "->  " + c.contentsName());
                        }else{
                            System.out.println(StringUtils.repeat(" ", repeat) + c.contentsName());
                        }
                    }
                }

                try {
                    Thread.sleep(100);
                }catch(InterruptedException ie){
                    ie.printStackTrace();
                    System.exit(0);
                }

            }

            mainMenu.contents.get(mainMenu.select).run();

        }catch (IOException ioe){
            ioe.printStackTrace();
        }


    }

    public static void main(String[] args){
        run();
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {

    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {

    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent e) {
        if(e.getRawCode() == 65362) { //上へ移動
            if(select == 0){
                return ;
            }
            select -= 1;
        }
        if(e.getRawCode() == 65364) { //下へ移動
            if(select == contents.size()-1){
                return ;
            }
            select += 1;
        }
        if(e.getRawCode() == 65293){
            flag = false;
        }

    }
}
