package com.block_break.hobby;

import com.block_break.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Round {
    private String str;
    private Point center;
    private Point pt;
    private List<Point> circle;
    private int cnt;

    public Round(String str, Point center){
        this.str = str;
        this.center = center;
        this.circle = new ArrayList<>(Arrays.asList(
                new Point(center.getX(), center.getY()),
                new Point(center.getX()+1, center.getY()),
                new Point(center.getX()+1, center.getY()+1),
                new Point(center.getX(), center.getY()+1)));
        this.cnt = 0;
    }

    public Point next(){
        Point nextPoint = circle.get(cnt);
        cnt = (cnt+1) % circle.size();

        return nextPoint;
    }

    public String put(){
        return str;
    }

}
