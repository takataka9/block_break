package com.block_break.hobby;

import jline.Terminal;
import jline.TerminalFactory;
import com.block_break.Point;
import org.apache.commons.lang3.StringUtils;

public class Loading {
    private Terminal terminal;
    private int height;
    private int width;
    private int max;
    private int cnt;
    private Round round;



    public Loading(){
        terminal = TerminalFactory.get();
        height = terminal.getHeight();
        width = terminal.getWidth();
        max = width - 15;
        cnt = 0;
        round = new Round(".", new Point((int)(width*0.7), (int)(height*0.7)));
    }

    public static void main(String[] args){
        Loading loading = new Loading();

        while(loading.cnt < loading.max){
            loading.cnt ++;
            loading.showDisplay();
            try {
                Thread.sleep(50);
            }catch(InterruptedException ie){

            }
        }
    }

    public static void run(){
        Loading loading = new Loading();

        while(loading.cnt < loading.max){
            loading.cnt ++;
            loading.showDisplay();
            try {
                Thread.sleep(50);
            }catch(InterruptedException ie){

            }
        }
    }

    public void showDisplay(){
        Point pt = round.next();
        for(int i=0;i<height;i++){
            if(i == pt.getY()){
                System.out.println(StringUtils.repeat(" ", pt.getX()-1) + round.put());
            }
            if(i == (int)(height*0.8)){
                System.out.printf("|%s>%s|%3d%%\n",
                        StringUtils.repeat("=", cnt),
                        StringUtils.repeat(' ', max-cnt),
                        (int)(cnt/(double)(max)*100));
                continue;
            }
            System.out.println("");
        }
    }

}
