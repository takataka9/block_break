package com.block_break;

public interface Contents {
    void run();
    String contentsName();
}
