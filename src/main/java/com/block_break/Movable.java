package com.block_break;

public interface Movable {
    Point next(Point p);
    Direction shift(double vec);
}
