package com.block_break;

import java.util.List;

public interface FieldObject {
    void collision(Ball ball);
    String putObject();
    int getLength();
    int getX();
    int getY();
    Point getPoint();
    List<Point> getArea();
}
