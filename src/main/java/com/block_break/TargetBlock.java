package com.block_break;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class TargetBlock implements FieldObject {
    private int length;
    private String block;
    private List<Point> points;
    private Point pt;
    private int left;
    private int right;

    public TargetBlock(int length, int center_x, int y){
        this.length = length;
        this.pt = new Point(center_x, y);
        this.points = new ArrayList<>();
        for(int i=pt.getX()-length/2;i<=pt.getX()+length/2;i++){
            points.add(new Point(i, y));
        }
        left = points.get(0).getX();
        right = points.get(length-1).getX();
        this.block = StringUtils.repeat('=', length);
    }

    public static List<TargetBlock> createBlocks(int height, int width, int pattern){
        List<TargetBlock> targets = new ArrayList<>();
        if(pattern == 0){
            int len = 5;
            int num = 6;
            int margin = (width - (len * num + (num - 1) * 3)) /2;
            for(int i=5;i<(int)(height*0.4);i++) {
                if(i % 3 == 0) {
                    for (int j = margin; j < width - margin; j += 8) {
                        targets.add(new TargetBlock(5, j, i));
                    }
                }
            }
        }

        return targets;
    }

    @Override
    public int getLength(){
        return length;
    }

    @Override
    public void collision(Ball ball){
        if(ball.getX() >= left && ball.getX() <= right){
            ball.reverseVecY();
        }else if(ball.getY() == pt.getY()){
            ball.reverseVecX();
        }else {
            ball.reverseVecX();
            ball.reverseVecY();
        }
        ball.collision(this, 0);

    }

    @Override
    public String putObject(){
        return block;
    }

    @Override
    public int getX() {
        return pt.getX();
    }

    @Override
    public int getY() {
        return pt.getY();
    }

    @Override
    public Point getPoint() {
        return pt;
    }

    @Override
    public List<Point> getArea(){
        return points;

    }
}

