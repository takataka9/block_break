package com.block_break;

import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

public class Controller implements NativeKeyListener {
    private PlayerBlock player;
    private int width;

    public Controller(PlayerBlock player, int width){
        this.player = player;
        this.width = width;
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent e){

    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent e){

    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent e) {
        if (e.getRawCode() == 65363) { //右へ移動
            if (player.getRight().getX() < width - 2) {
                player.move(1);
            }
        }
        if (e.getRawCode() == 65361) { //左へ移動
            if (player.getLeft().getX() > 2) {
                player.move(-1);
            }
        }

    }
}

